import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { databaseProviders } from './databse/database.provider';
import { IdentityProviders } from './databse/identity.providers';
import { IdentifyController } from './identify.controller';
import { IdentifyService } from './identify.service';
import { ExistsStrategy } from './strategies/exists.strategy';
import { JwtStrategy } from './strategies/jwt.strategy';
import { LocalStrategy } from './strategies/local.strategy';

@Module({
  imports: [
    PassportModule,
    JwtModule.register({
      secret : 'alibadsaba',
      signOptions : {expiresIn : '600s'}
    })
  ],
  controllers: [IdentifyController],
  providers: [...IdentityProviders,IdentifyService,...databaseProviders,LocalStrategy,JwtStrategy,ExistsStrategy],
  exports: [...databaseProviders],
})
export class IdentifyModule {}
 