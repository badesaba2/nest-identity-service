import {ExtractJwt,Strategy} from "passport-jwt"
import { PassportStrategy } from "@nestjs/passport";
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { LoginDto } from "../dto/login.dto";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy,'jwt') {

  constructor() {
        super({
            jwtFromRequest : ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration : false,
            secretOrKey : 'alibadsaba'
        });
  }

  async validate(payload:any): Promise<any> {

    console.log('payload',payload);

    return payload;


}

}