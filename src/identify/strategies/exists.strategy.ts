import { Strategy } from "passport-local";
import { PassportStrategy } from "@nestjs/passport";
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { IdentifyService } from "../identify.service";
import { LoginDto } from "../dto/login.dto";
import { UserAlreadyExistsException } from "../exceptions/UserAlreadyExists.exception";

@Injectable()
export class ExistsStrategy extends PassportStrategy(Strategy,'exists') {
  constructor(private IdentifyService: IdentifyService) {
    super();
  }

  async validate(username: string, password: string): Promise<any> {

    console.log(username);
     

    const user = await this.IdentifyService.getUserByUsername(username);


    if (user || user!==null) {
      throw new UserAlreadyExistsException();
    }
    return {
      username  :username,
      password  : password
    };
  } 


}