import { Strategy } from "passport-local";
import { PassportStrategy } from "@nestjs/passport";
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { IdentifyService } from "../identify.service";
import { LoginDto } from "../dto/login.dto";

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy,'local') {
  constructor(private IdentifyService: IdentifyService) {
    super();
  }

  async validate(username: string, password: string): Promise<any> {

    let logindto : LoginDto ={
        username,password
    }

    const user = await this.IdentifyService.validateUser(logindto);


    if (!user || user===null) {
      throw new UnauthorizedException();
    }
    return user;
  } 


}