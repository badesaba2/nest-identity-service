import { Controller, UseGuards } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { IdentifyService } from './identify.service';
import { ExistsAuthGuard } from './strategies/exists-auth.guard';
import { JwtAuthGuard } from './strategies/jwt-auth.guard';
import { LocalAuthGuard } from './strategies/local-auth.guard';

@Controller('identify')
export class IdentifyController {

    constructor(private identifyService : IdentifyService){}

    @MessagePattern('msg')
    hello(req){
        console.log(req);
        this.identifyService.hello(req)
    }

    @UseGuards(ExistsAuthGuard)
    @MessagePattern('register')
    async register(command){
        return this.identifyService.register(command.body);
    }   

    @UseGuards(LocalAuthGuard)
    @MessagePattern('login')
    async logIn(command){
        return this.identifyService.logIn({
            ...command.user
        //for set admin test    // ,roles:['admin']
        });
    }


    @UseGuards(JwtAuthGuard)
    @MessagePattern('me')
    async me(command){
        const {id,...rest} = command.user;
        return rest;
    }

    @MessagePattern('isAuthed')
    async isAuthed(command){
        try{
            const res = this.identifyService.validateToken(command.jwt); 
            return res;    
        }
        catch(err){}
    }
    
}
