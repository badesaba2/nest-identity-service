import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CreateIdentityDto } from './dto/createIdentity.dto';
import { LoginDto } from './dto/login.dto';
import { TokenDto } from './dto/token.dto';
import { Identity } from './interfaces/Identify';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class IdentifyService {

    constructor( 
        @Inject('IDENTITY_MODEL')
        private identityModel: Model<Identity>,
        private jwtService : JwtService
    ){}

    hello (message){
        return message;
    }

    async register(createIdentityDto : CreateIdentityDto){

        const createdIdentity = new this.identityModel(createIdentityDto);
        let res = await createdIdentity.save();
        console.log(res);
        return res;

    }


    async getUserByUsername(username : string){

        let res = await this.identityModel.findOne({
            username : username,
        });

        if(!res) return null;

        let json = res.toObject(); 
        let {__v , _id,...userdata} = json;
         

        return {id:json._id, ...userdata}
    }

    async validateUser(loginDto : LoginDto){

        let res = await this.identityModel.findOne({
            username : loginDto.username,
            password : loginDto.password
        });

        if(!res) return null;

        let json = res.toObject(); 
        let {__v , _id,...userdata} = json;
         

        return {id:json._id, ...userdata}
    }


     validateToken (jwt : string){
        const validateToken = this.jwtService.verify(jwt);
        return  validateToken;

    }

    async logIn(user : any){

        let payload ={
              id:user._id,
              name : user.name,
              username : user.username ,
              roles : user.roles 
        };

        var token = this.jwtService.sign(payload);
        var tokenValue: any = this.jwtService.decode(token);

        // var date = new Date(tokenValue.exp*1000);

        // var refTokenData = new Date(
        //       date.setDate(date.getDate()+1)
        // ).toISOString();

        // const tokenData : TokenDto ={
        //     token : token,
        //     expiresIn : tokenValue.exp,
        //     refreshTokenExpiresIn :  refTokenData,
        //     expired : false
        // }    

        return {
            access_toekn : this.jwtService.sign(payload),
            expires_in : tokenValue.exp
        };
    }


}
