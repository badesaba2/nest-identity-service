import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';

async function bootstrap() {

  const microserviceOptions : MicroserviceOptions = { 

    transport : Transport.NATS,
    options:{
      urls : ['nats://localhost:4222'],
      queue : 'account_service_queue'
    }

  }

  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    microserviceOptions
  );

  await app.listen();
}

bootstrap();
